/* @Class Name   : ConstantsUtility
 * @Description  : Utility class to reference all string literal/constants that are being used in the organisation
 * @Created By   : Abhinav Gadepalli
 * @Created On   : 10/4/2014
 * @Modification Log:  
 * -------------------------------------------------------------------------------------------------------
 * @Developer                Date                 Description 
 * -------------------------------------------------------------------------------------------------------
 * @Abhinav G         		 10/23/2014           Created
 * --------------------------------------------------------------------------------------------------------
 */
public with sharing class ConstantsUtility {
	public static String ALL = 'All';
	public static String ANY_CATEGORY = '--Any Category--';
	public static String DRAFT='Draft';
	public static String LKID = '_lkid';
	public static String CF = 'CF';
	public static String STORE = 'Store: ';
	public static String PONAME = 'Purchase Order Name: ';
	public static String BUDGET = 'Budget: ';
	
}