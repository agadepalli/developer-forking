/* @Class Name   : OrderLineItemTriggerHelper
 * @Description  : Helper class for the trigger on Line_Item__c object that contains method to achieve the following functionalities:
 					1. Update the purchased amount on the order to roll up the amount spent on line items
 					   when the line items are inserted/updated/deleted/undeleted
 					2. Update the top product cateogory related fields on order when line items are created/updated/deleted/undeleted
 * @Created By   : Abhinav Gadepalli
 * @Created On   : 10/2/2014
 * @Modification Log:  
 * -------------------------------------------------------------------------------------------------------
 * @Developer                Date                 Description 
 * -------------------------------------------------------------------------------------------------------
 * @Abhinav G         		 10/02/2014           Created
 * --------------------------------------------------------------------------------------------------------
 */
public with sharing class OrderLineItemTriggerHelper {
	
  /**    
    * @Description: Method to update the Purchased amount and fields for top two product cateogires and their respective units per order when line items are created/updated/deleted/undeleted
    * @Param: triger.new list from the trigger
    * @Return: none
    */
	public static void updateOrders(List<Line_Item__c> newTriggerList){ 
		
		Set<Id> purchaseOrderIds = new Set<Id>(); //Set of Ids to hold all the unique order Ids of the line items from trigger
		Map<Id,List<Line_Item__c>> orderToLineItemsMap = new Map<Id,List<Line_Item__c>>();//Map with order as key and list of Line Items as value. This map is built to add Error Message to appropriate trigger records when the update DML on order fails
		List<Order__c> ordersForUpdate = new List<Order__c>();
		Decimal purchasedAmount = 0;
		try{
			for(Line_Item__c li : newTriggerList){//Build the required map from incoming line item records 
				
				if(li.Order__c <> null){
					purchaseOrderIds.add(li.Order__c);
					List<Line_Item__c> tempItemList = new List<Line_Item__c>();
					if(orderToLineItemsMap.get(li.Order__c) == null){
						tempItemList.add(li);
					}
					else{
						tempItemList = orderToLineItemsMap.get(li.Order__c);
						tempItemList.add(li);
					}
					orderToLineItemsMap.put(li.Order__c,tempItemList);					
				}
			}
			Integer limitResults = Limits.getLimitQueryRows() - Limits.getQueryRows();
			//Single aggregate query to get the amount, units categorised by order and product category
			List<AggregateResult> agr = [Select Sum(Amount__c) sum,sum(Units__c) cnt,Lot__r.Product__c pName,Order__c odr FROM Line_Item__c
																							 WHERE Order__c IN :purchaseOrderIds
																							 GROUP BY Order__c,Lot__r.Product__c
																							 ORDER BY sum(Units__c) desc,Lot__r.Product__c desc LIMIT :limitResults];
			
			//Map to hold the mapping of list of aggregate result records per order for bulkify trigger to handle multiple line items which belong to different orders
			Map<String,List<AggregateResult>> mapAgr = new Map<String,List<AggregateResult>>();
		    for(AggregateResult a :agr){
		        List<AggregateResult> tempList = new List<AggregateResult>();
		        if(mapAgr.get((String)a.get('odr')) <> null){
		            tempList = mapAgr.get((String)a.get('odr'));
		            tempList.add(a);
		        }
		        else{
		            tempList.add(a);
		        }
		        mapAgr.put((String)a.get('odr'),tempList);
		    }
		    for(String s:mapAgr.keySet()){
		    		purchasedAmount = 0;
		    		purchaseOrderIds.remove(Id.valueOf(s));
		    		for(AggregateResult a: mapAgr.get(s)){
		        		purchasedAmount = purchasedAmount + (Decimal)a.get('sum');
		        	}
		        	Order__c o = new Order__c(Id=Id.valueOf(s),Purchased__c=purchasedAmount,Product_Category_1__c=String.valueOf(mapAgr.get(s)[0].get('pName')),Product_Category_Units_1__c=Integer.valueOf(mapAgr.get(s)[0].get('cnt')));
		            if(mapAgr.get(s).size()>1){
		                o.Product_Category_2__c = String.valueOf(mapAgr.get(s)[1].get('pName'));
		            	o.Product_Category_Units_2__c = Integer.valueOf(mapAgr.get(s)[1].get('cnt'));  
		            }
		            else{
		            	o.Product_Category_2__c = null;
		            	o.Product_Category_Units_2__c = null;
		            }	
		            ordersforUpdate.add(o);       	 
		    }
		    //The below loop ensures that the orders without any line items left are also updated appropriately(as the trigger context is after delete, orders without any items are not picked in the aggregate query)
		    for(Id orderId : purchaseOrderIds){
				Order__c o = new Order__c(id=orderId,Product_Category_1__c=null,Product_Category_Units_1__c=null,Product_Category_2__c=null,Product_Category_Units_2__c=null,Purchased__c=0);
				ordersforUpdate.add(o); 
			}	
		    															   
			List<Database.SaveResult> srList = Database.update(ordersForUpdate,false);	//update all the orders in the current context with single scenario	
			for(Integer i = 0; i < srList.size(); i++){//For orders that failed during update, add error to the line item record so that such line items are not loaded with bad data
				if(!srList[i].isSuccess()){
					for(Line_Item__c li : orderToLineItemsMap.get(ordersForUpdate[i].Id))
						li.addError(srList[i].getErrors()[0].getMessage());
				}
			}																					 
		} 	
		catch(Exception e){
			for(Line_Item__c li: newTriggerList){
				System.debug('Exception Message : '+e.getMessage()+' Exception stack trace : '+e.getStackTraceString());
				li.addError('An unexpected error has occured while processing your transaction. Please contact your administrator for assistance');
			}
		}
	}
}