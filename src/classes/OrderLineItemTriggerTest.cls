/** 
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition. 
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
 
@isTest(seeAllData=false) 
private class OrderLineItemTriggerTest { 
	/*
	 * Method to test the insert scenario of single line item record per order
	*/
	static testmethod void insertTriggerTest_SingleLineItem(){ 
		User testAccountManager = UnitTestHelper.reusableUser('Account Managers','testAM');
		insert testAccountManager;
		User testInventoryManager = UnitTestHelper.reusableUser('Inventory Managers','testIM');
		insert testInventoryManager;
		Lot__c prod1;
		Lot__c prod2;
		System.runAs(testInventoryManager){
			prod1 = UnitTestHelper.reusableProductLot(100,5,'Batteries','HTC');
			insert prod1;
			prod2 = UnitTestHelper.reusableProductLot(250,25,'Case Covers','Iphone Silicone Case');
			insert prod2;
		}
		System.runAs(testAccountManager){
			
			Account acc = UnitTestHelper.reusableAccount();
			insert acc;
			Order__c odr = UnitTestHelper.reusablePurchaseOrder('Order 1',acc.Id,100000, 'Open');
			insert odr;
			Test.startTest();
				Line_Item__c lItem1 = UnitTestHelper.reusableLineItem(prod1.Id,odr.Id,50); 
				insert lItem1;
			Test.stopTest();
			Order__c purchaseOrder = [Select Remaining__c,Product_Category_1__c,Product_Category_2__c,Product_Category_Units_1__c,Product_Category_Units_2__c,Purchased__c from Order__C where Id =:odr.Id];
			System.assertEquals(purchaseOrder.Product_Category_1__c,'HTC');
			System.assertEquals(purchaseOrder.Product_Category_2__c,null);
			System.assertEquals(purchaseOrder.Product_Category_Units_1__c,50);
			System.assertEquals(purchaseOrder.Product_Category_Units_2__c,null);
			System.assertEquals(purchaseOrder.Remaining__c,99750);
			System.assertEquals(purchaseOrder.Purchased__c,250);	
		}
	}
	/*
	 * Method to test the insert scenario of multiple line items per purchase order and assert the population of top product category fields
	*/
	static testmethod void insertTriggerTest_MultipleLineItems(){ 
		User testAccountManager = UnitTestHelper.reusableUser('Account Managers','testAM');
		insert testAccountManager;
		User testInventoryManager = UnitTestHelper.reusableUser('Inventory Managers','testIM');
		insert testInventoryManager;
		Lot__c prod1;
		Lot__c prod2;
		System.runAs(testInventoryManager){
			prod1 = UnitTestHelper.reusableProductLot(100,5,'Batteries','HTC');
			insert prod1;
			prod2 = UnitTestHelper.reusableProductLot(250,25,'Case Covers','Iphone Silicone Case');
			insert prod2;
		}
		System.runAs(testAccountManager){
			Account acc = UnitTestHelper.reusableAccount();
			insert acc;
			Order__c odr = UnitTestHelper.reusablePurchaseOrder('Order 1',acc.Id,100000, 'Open');
			insert odr;
			Test.startTest();
				Line_Item__c lItem1 = UnitTestHelper.reusableLineItem(prod1.Id,odr.Id,50); 
				insert lItem1;
				Line_Item__c lItem2 = UnitTestHelper.reusableLineItem(prod2.Id,odr.Id,150); 
				insert lItem2;
			Test.stopTest();
			Order__c updatedPurchaseOrder = [Select Remaining__c,Product_Category_1__c,Product_Category_2__c,Product_Category_Units_1__c,Product_Category_Units_2__c,Purchased__c from Order__C where Id =:odr.Id];
			System.assertEquals(updatedPurchaseOrder.Product_Category_1__c,'Iphone Silicone Case');
			System.assertEquals(updatedPurchaseOrder.Product_Category_2__c,'HTC');
			System.assertEquals(updatedPurchaseOrder.Product_Category_Units_1__c,150);
			System.assertEquals(updatedPurchaseOrder.Product_Category_Units_2__c,50);
			System.assertEquals(updatedPurchaseOrder.Remaining__c,96000);
			System.assertEquals(updatedPurchaseOrder.Purchased__c,4000);	
		}
	}
	/*
	 * Method to test the update scenario of line items
	*/
	static testmethod void updateTriggerTest(){ 
		User testAccountManager = UnitTestHelper.reusableUser('Account Managers','testAM');
		insert testAccountManager;
		User testInventoryManager = UnitTestHelper.reusableUser('Inventory Managers','testIM');
		insert testInventoryManager;
		Lot__c prod1;
		Lot__c prod2;
		System.runAs(testInventoryManager){
			prod1 = UnitTestHelper.reusableProductLot(100,5,'Batteries','HTC');
			insert prod1;
			prod2 = UnitTestHelper.reusableProductLot(250,25,'Case Covers','Iphone Silicone Case');
			insert prod2;
		}
		System.runAs(testAccountManager){
			
			Account acc = UnitTestHelper.reusableAccount();
			insert acc;
			Order__c odr = UnitTestHelper.reusablePurchaseOrder('Order 1',acc.Id,100000, 'Open');
			insert odr;
			Test.startTest();
				Line_Item__c lItem = UnitTestHelper.reusableLineItem(prod1.Id,odr.Id,50); 
				insert lItem;
			    Order__c purchaseOrder = [Select Remaining__c,Product_Category_1__c,Product_Category_2__c,Product_Category_Units_1__c,Product_Category_Units_2__c,Purchased__c from Order__C where Id =:odr.Id];
				System.assertEquals(purchaseOrder.Product_Category_1__c,'HTC');
				System.assertEquals(purchaseOrder.Product_Category_2__c,null);
				System.assertEquals(purchaseOrder.Product_Category_Units_1__c,50);
				System.assertEquals(purchaseOrder.Product_Category_Units_2__c,null);
				System.assertEquals(purchaseOrder.Remaining__c,99750);
				System.assertEquals(purchaseOrder.Purchased__c,250);	
				
				lItem.Units__c = 75;
				update lItem;
			Test.stopTest();
			Order__c updatedPurchaseOrder = [Select Remaining__c,Product_Category_1__c,Product_Category_2__c,Product_Category_Units_1__c,Product_Category_Units_2__c,Purchased__c from Order__C where Id =:odr.Id];
			System.assertEquals(updatedPurchaseOrder.Product_Category_1__c,'HTC');
			System.assertEquals(updatedPurchaseOrder.Product_Category_2__c,null);
			System.assertEquals(updatedPurchaseOrder.Product_Category_Units_1__c,75);
			System.assertEquals(updatedPurchaseOrder.Product_Category_Units_2__c,null);
			System.assertEquals(updatedPurchaseOrder.Remaining__c,99625);
			System.assertEquals(updatedPurchaseOrder.Purchased__c,375);
		}	
		
	}
	/*
	 * Method to test the delete scenario of line items
	*/
	static testmethod void deleteTriggerTest(){ 
		User testAccountManager = UnitTestHelper.reusableUser('Account Managers','testAM');
		insert testAccountManager;
		User testInventoryManager = UnitTestHelper.reusableUser('Inventory Managers','testIM');
		insert testInventoryManager;
		Lot__c prod1;
		Lot__c prod2;
		System.runAs(testInventoryManager){
			prod1 = UnitTestHelper.reusableProductLot(100,5,'Batteries','HTC');
			insert prod1;
			prod2 = UnitTestHelper.reusableProductLot(250,25,'Case Covers','Iphone Silicone Case');
			insert prod2;
		}
		System.runAs(testAccountManager){
			Account acc = UnitTestHelper.reusableAccount();
			insert acc;
			Order__c odr = UnitTestHelper.reusablePurchaseOrder('Order 1',acc.Id,100000, 'Open');
			insert odr;
			Test.startTest();
				Line_Item__c lItem = UnitTestHelper.reusableLineItem(prod1.Id,odr.Id,50); 
				insert lItem;
				delete lItem;
			Test.stopTest();
			Order__c updatedPurchaseOrder = [Select Remaining__c,Product_Category_1__c,Product_Category_2__c,Product_Category_Units_1__c,Product_Category_Units_2__c,Purchased__c from Order__C where Id =:odr.Id];
			System.assertEquals(updatedPurchaseOrder.Product_Category_1__c,null);
			System.assertEquals(updatedPurchaseOrder.Product_Category_2__c,null);
			System.assertEquals(updatedPurchaseOrder.Product_Category_Units_1__c,null);
			System.assertEquals(updatedPurchaseOrder.Product_Category_Units_2__c,null);
			System.assertEquals(updatedPurchaseOrder.Remaining__c,100000);
			System.assertEquals(updatedPurchaseOrder.Purchased__c,0);	
		} 
	}
	/*
	 * Method to test the undelete scenario of line items
	*/
	static testmethod void undeleteTriggerTest(){ 
		User testAccountManager = UnitTestHelper.reusableUser('Account Managers','testAM');
		insert testAccountManager;
		User testInventoryManager = UnitTestHelper.reusableUser('Inventory Managers','testIM');
		insert testInventoryManager;
		Lot__c prod1;
		Lot__c prod2;
		System.runAs(testInventoryManager){
			prod1 = UnitTestHelper.reusableProductLot(100,5,'Batteries','HTC');
			insert prod1;
			prod2 = UnitTestHelper.reusableProductLot(250,25,'Case Covers','Iphone Silicone Case');
			insert prod2;
		}
		System.runAs(testAccountManager){
			Account acc = UnitTestHelper.reusableAccount();
			insert acc;
			Order__c odr = UnitTestHelper.reusablePurchaseOrder('Order 1',acc.Id,100000, 'Open');
			insert odr;
			Test.startTest();
				Line_Item__c lItem = UnitTestHelper.reusableLineItem(prod1.Id,odr.Id,50); 
				insert lItem;
				delete lItem;
				Order__c purchaseOrder = [Select Remaining__c,Product_Category_1__c,Product_Category_2__c,Product_Category_Units_1__c,Product_Category_Units_2__c,Purchased__c from Order__C where Id =:odr.Id];
				System.assertEquals(purchaseOrder.Product_Category_1__c,null);
				System.assertEquals(purchaseOrder.Product_Category_2__c,null);
				System.assertEquals(purchaseOrder.Product_Category_Units_1__c,null);
				System.assertEquals(purchaseOrder.Product_Category_Units_2__c,null);
				System.assertEquals(purchaseOrder.Remaining__c,100000);
				System.assertEquals(purchaseOrder.Purchased__c,0);	
				undelete lItem;
			Test.stopTest();
			Order__c updatedPurchaseOrder = [Select Remaining__c,Product_Category_1__c,Product_Category_2__c,Product_Category_Units_1__c,Product_Category_Units_2__c,Purchased__c from Order__C where Id =:odr.Id];
			System.assertEquals(updatedPurchaseOrder.Product_Category_1__c,'HTC');
			System.assertEquals(updatedPurchaseOrder.Product_Category_2__c,null);
			System.assertEquals(updatedPurchaseOrder.Product_Category_Units_1__c,50);
			System.assertEquals(updatedPurchaseOrder.Product_Category_Units_2__c,null);
			System.assertEquals(updatedPurchaseOrder.Remaining__c,99750);
			System.assertEquals(updatedPurchaseOrder.Purchased__c,250);	
		}
	}
	/*
	 * Method to test the bulk insert of line items to make sure that the triger is bulkified and governor limits are not reached during bulk data loads
	*/
	static testmethod void bulkifiedInsertTriggerTest_multipleOrders(){
		User testAccountManager = UnitTestHelper.reusableUser('Account Managers','testAM');
		insert testAccountManager;
		User testInventoryManager = UnitTestHelper.reusableUser('Inventory Managers','testIM');
		insert testInventoryManager;
		List<Lot__c> batteryProducts;
		System.runAs(testInventoryManager){
			batteryProducts = new List<Lot__c>();
			for(Integer i=0;i<2000;i++){
				Lot__c prod1 = UnitTestHelper.reusableProductLot(100,5,'Batteries','HTC'+String.valueOf(i));
				batteryProducts.add(prod1);
			}
			insert batteryProducts;
		}
		System.runAs(testAccountManager){
			
			List<Line_Item__c> batteryListItems = new List<Line_Item__c>();
			Account acc = UnitTestHelper.reusableAccount();
			insert acc;
			List<Order__c> listOrders = new List<Order__c>();
			for(Integer i =0; i < batteryProducts.size(); i++){
				Order__c odr = UnitTestHelper.reusablePurchaseOrder('Order 1',acc.Id,10000000, 'Draft');
				listOrders.add(odr);
			}
			insert listOrders;		
			
			for(Integer i =0;i<2000;i++){
				Line_Item__c lItem1 = UnitTestHelper.reusableLineItem(batteryProducts[i].Id,listOrders[i].Id,50); 
				batteryListItems.add(lItem1);
			}
			
			Test.startTest();
				insert batteryListItems;
				List<Line_Item__c> insertedLineItems = [Select Id from Line_Item__c];
				System.assertEquals(insertedLineItems.size(),2000);
		    Test.stopTest();    	    
		}
	}
	/*
	 * Method to test the bulk insert of line items to make sure that the triger is bulkified and governor limits are not reached during bulk data loads
	*/
	static testmethod void bulkifiedInsertTriggerTest_singleOrder(){
		User testAccountManager = UnitTestHelper.reusableUser('Account Managers','testAM');
		insert testAccountManager;
		User testInventoryManager = UnitTestHelper.reusableUser('Inventory Managers','testIM');
		insert testInventoryManager;
		List<Lot__c> batteryProducts;
		System.runAs(testInventoryManager){
			batteryProducts = new List<Lot__c>();
			for(Integer i=0;i<2000;i++){
				Lot__c prod1 = UnitTestHelper.reusableProductLot(100,5,'Batteries','HTC'+String.valueOf(i));
				batteryProducts.add(prod1);
			}
			insert batteryProducts;
		}
		System.runAs(testAccountManager){
			
			List<Line_Item__c> batteryListItems = new List<Line_Item__c>();
			Account acc = UnitTestHelper.reusableAccount();
			insert acc;
			Order__c odr = UnitTestHelper.reusablePurchaseOrder('Order 1',acc.Id,10000000, 'Draft');
			insert odr;		
			
			for(Integer i =0;i<2000;i++){
				Line_Item__c lItem1 = UnitTestHelper.reusableLineItem(batteryProducts[i].Id,odr.Id,50); 
				batteryListItems.add(lItem1);
			}
			
			Test.startTest();
				insert batteryListItems;
				List<Line_Item__c> insertedLineItems = [Select Id from Line_Item__c where Order__c =:odr.Id];
				System.assertEquals(insertedLineItems.size(),2000);
		    Test.stopTest();    	    
		}
	}
	/*
	 * Method to test the bulk update of line items to make sure that the triger is bulkified and governor limits are not reached during bulk data loads
	*/
	static testmethod void bulkifiedUpdateTriggerTest(){
		User testAccountManager = UnitTestHelper.reusableUser('Account Managers','testAM');
		insert testAccountManager;
		User testInventoryManager = UnitTestHelper.reusableUser('Inventory Managers','testIM');
		insert testInventoryManager;
		List<Lot__c> batteryProducts;
		System.runAs(testInventoryManager){
			batteryProducts = new List<Lot__c>();
			for(Integer i=0;i<2000;i++){
				Lot__c prod1 = UnitTestHelper.reusableProductLot(100,5,'Batteries','HTC'+String.valueOf(i));
				batteryProducts.add(prod1);
			}
			insert batteryProducts;
		}
		System.runAs(testAccountManager){
			
			List<Line_Item__c> batteryListItems = new List<Line_Item__c>();
			Account acc = UnitTestHelper.reusableAccount();
			insert acc;
			Order__c odr = UnitTestHelper.reusablePurchaseOrder('Order 1',acc.Id,10000000, 'Draft');
			insert odr;		
			List<Line_Item__c> updateLineItems = new List<Line_Item__c>();
			for(Integer i =0;i<2000;i++){
				Line_Item__c lItem1 = UnitTestHelper.reusableLineItem(batteryProducts[i].Id,odr.Id,50); 
				batteryListItems.add(lItem1);
			}
			insert batteryListItems;
			Test.startTest();				
				for(Line_Item__c li: [Select Id,Units__c from Line_Item__c where Order__c =:odr.Id]){
					li.Units__c = 50;
					updateLineItems.add(li);
				}
				update updateLineItems;
				for(Line_Item__c li : [Select Units__c from Line_Item__c where Order__c =:odr.Id]){
					System.assertEquals(li.Units__c,50);
				}
		    Test.stopTest();    	    
		}
	}
	
	/*
	 * Method to test the bulk delete of line items to make sure that the triger is bulkified and governor limits are not reached during bulk data loads
	*/
	static testmethod void bulkifiedDeleteTriggerTest(){
		User testAccountManager = UnitTestHelper.reusableUser('Account Managers','testAM');
		insert testAccountManager;
		User testInventoryManager = UnitTestHelper.reusableUser('Inventory Managers','testIM');
		insert testInventoryManager;
		List<Lot__c> batteryProducts;
		System.runAs(testInventoryManager){
			batteryProducts = new List<Lot__c>();
			for(Integer i=0;i<2000;i++){
				Lot__c prod1 = UnitTestHelper.reusableProductLot(100,5,'Batteries','HTC'+String.valueOf(i));
				batteryProducts.add(prod1);
			}
			insert batteryProducts;
		}
		System.runAs(testAccountManager){
			
			List<Line_Item__c> batteryListItems = new List<Line_Item__c>();
			Account acc = UnitTestHelper.reusableAccount();
			insert acc;
			Order__c odr = UnitTestHelper.reusablePurchaseOrder('Order 1',acc.Id,10000000, 'Draft');
			insert odr;		
			List<Line_Item__c> deleteLineItems = new List<Line_Item__c>();
			for(Integer i =0;i<2000;i++){
				Line_Item__c lItem1 = UnitTestHelper.reusableLineItem(batteryProducts[i].Id,odr.Id,50); 
				batteryListItems.add(lItem1);
			}
			insert batteryListItems;
			Test.startTest();				
				for(Line_Item__c li: [Select Id,Units__c from Line_Item__c where Order__c =:odr.Id]){
					deleteLineItems.add(li);
				}
				delete deleteLineItems;
				System.assertEquals([Select Id from Line_Item__c where Order__c =:odr.Id].size(),0);
		    Test.stopTest();    	    
		}
	}	
}