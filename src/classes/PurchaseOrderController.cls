/* @Class Name   : PurchaseOrderController
 * @Description  : Controller for PurchaseOrder visualforce page. This controler has business logic to display paginated 
 				   product lot/line items, calculate summary statistics, insert/update purchase orders and associate appropriate
 				   lineitems, enforce validations and data completeness for orders.
 * @Created By   : Abhinav Gadepalli
 * @Created On   : 10/4/2014
 * @Modification Log:  
 * -------------------------------------------------------------------------------------------------------
 * @Developer                Date                 Description 
 * -------------------------------------------------------------------------------------------------------
 * @Abhinav G         		 10/04/2014           Created
 * --------------------------------------------------------------------------------------------------------
 */
public with sharing class PurchaseOrderController {
	
	// Variable declaration Section
    public Order__c purchaseOrder {set; get;} //Instance of the Order object that is the current scope of the page 
    @TestVisible private Map<Id,Integer> unitsPerProductMap; //Map variable to maintain the user entered values units for each product in UI
    public Id orderId {set; get;} //Id variable to hold the record Id of the purchase order of the current page
    public String productCategory{get; set;} //Variable to capture the user selection from the Product Category picklist on the purchase order new/edit page
    private final Integer pageSize = 5;    //  Number of row items in the paginated list items on the VF Page
    //variable to hold data that is currenctly being dispalyed in the list items section in the pageblocktable
    public  List<LineItemsWrapper> currentPage  { get; set;}
	//boolean variable which controls the rendering of "Previous" button of paginated table based on the pagenumber that is currently being displayed
    public  Boolean hasPrevious         { get { 
                                                return recordList.getHasPrevious(); 
                                              } 
                                        }
    //boolean variable which controls the rendering of "Next" button of paginated table based on the pagenumber that is currently being displayed
    public  Boolean hasNext             { get { 
                                                return recordList.getHasNext(); 
                                              } 
                                        }
    private  List<LineItemsWrapper> listWrappers;
    public Integer numOfPages {set; get;}           //Integer to capture the number of record sets of a selected product category/'All' line items
    public Integer pageNumber {set; get;}           //Integer to hold the current page number among the paginated result set
    public Integer noOfUnits {set; get;}            //Integer to hold the Total Number of Units in the purchase order(to be displayed in the Summary Section) 
    public Decimal perUnitPrice {set; get;}			//Decimal to hold the Per Unit Price of all the units in the purchase order (to be displayed in the Summary Section)
    private Map<String,List<Lot__c>> lineItemsMap;
    private Set<String> productCategories; //Set to hold the product categories to be added to the cateogry filter on UI
    public String orderStatus {set; get;}
    private Id storeId; 
    private transient SavePoint sp;
    public ApexPages.standardSetController recordList{get;set;}
    private List<Lot__c> productLotRecords;
    public PurchaseOrderController(ApexPages.StandardController sc) { 
        try{
        	//Initialisation variables with default values during page load
            pageNumber = 0;
            numOfPages=0;
            unitsPerProductMap = new Map<Id,Integer>();
            productCategories = new Set<String>();
            productCategory = ConstantsUtility.ALL; //Default product category during page load
            if(sc<>null && sc.getId()<>null){ //If the purchase order is an existing one, retrive details from database
                purchaseOrder = [Select Id,Name,Status__c,Budget__c,Store__c from Order__c where Id =:sc.getId()];
                orderId = purchaseOrder.Id; 
                orderStatus = purchaseOrder.Status__c;                           
            }
            else{ //If the purchase order is a new orrder, set default status as "Draft"
            	storeId = ApexPages.currentPage().getParameters().get(ConstantsUtility.CF+Label.Store_FieldID+ConstantsUtility.LKID);                
                purchaseOrder = new Order__c();   
                purchaseOrder.Status__c = ConstantsUtility.DRAFT;
                purchaseOrder.Store__c = storeId;
                orderStatus = ConstantsUtility.DRAFT;
            }
            //method call to get all product records, find matching line item records(if any) and build unitsPerProduct map to hold/keep updating user input units
            getAllLineItems();
            productCategories.addAll(lineItemsMap.keySet());//Create a set of all product categories so that it can be added to the  filter dropdown(the map cannot be reused as it will  be transient to avoid viewstate issues)
        	calculateSummaryStats();
      }
      catch(Exception e){
      	//The debug statement below helps the admin/developer to capture the exact cause of failure when the  user tries to reproduce the issue
      	System.debug('Exception Message : '+e.getMessage()+' Exception stack trace : '+e.getStackTraceString());
    	ApexPages.Message Msg = new ApexPages.Message(ApexPages.Severity.Error,Label.Unexpected_Error_Message); 
	    ApexPages.addMessage(Msg);
      }
    }
  /**    
    * @Description: Method to retrive all the product lots and map with any existing line items records to build map to hold units for each product
    * @Param: none
    * @Return: none
    */
    public void getAllLineItems(){
        listWrappers = new List<LineItemsWrapper>();
        //Map to store all the product lot records before existing line items are mapped to display appropriate units on UI
        productLotRecords = [Select Id,Name,Product__c,Unit_Price__c,Product_Category__c,Remaining__c,(Select Id,Units__c,Lot__c,Order__c FROM Line_Items__r WHERE Order__c =:orderId ) FROM Lot__c 
        																																ORDER BY Product__c asc]; 
        
        for(Lot__c lot : productLotRecords){ 
        	if(lot.Line_Items__r.size() > 0){
        		unitsPerProductMap.put(lot.Id,Integer.valueOf(lot.Line_Items__r[0].Units__c));
        	}
        	else{
        		unitsPerProductMap.put(lot.Id,0);
        	}
        }
        buildLineItemsMap(productLotRecords);
        recordList= new ApexPages.standardsetController(lineItemsMap.get(ConstantsUtility.ALL)); //StandardSetController used to use out of the box pagination  
        recordList.setPageSize(pageSize);//out-of-the-box set controller method for pagination 
        pageNumber = recordList.getPageNumber();
		numOfPages = Integer.valueOf(Math.ceil(Decimal.valueOf(recordList.getResultSize())/Decimal.valueOf(recordList.getPageSize())));
        buildWrapperList(recordList.getRecords());
    }
    
    /**    
    * @Description: Method to build the product-category wise line item wrapper records to be displayed in the UI 
    * @Param: List of all product lot records with the child records(line items for the current order)
    * @Return: none
    */
    public void buildLineItemsMap(List<Lot__c> productRecords){
        List<Lot__c> allProducts = new List<Lot__c>();
        lineItemsMap = new Map<String,List<Lot__c>>();
        //Iterate the incoming total list of product lot records to buid product-category based map with list of all product lot records as value
        for(Lot__c lot : productRecords){
        	if(lot.Line_Items__r.size() > 0 || lot.Remaining__c <> 0){
	            List<Lot__c > tempLineItem = new List<Lot__c >();
	            if(lineItemsMap.get(lot.Product_Category__c)<>null){//If an entry already exists in the map, add to the existing list of records
	                tempLineItem = lineItemsMap.get(lot.Product_Category__c);
	                tempLineItem.add(lot);
	            }
	            else{//If no entry found in the map, create a new list, add the record 
	                tempLineItem = new List<Lot__c>();
	                tempLineItem .add(lot);
	            }
	            lineItemsMap.put(lot.Product_Category__c,tempLineItem);//build the map with product category as the key and the prepared list of product lot records as values
        		allProducts.add(lot);
        	}
       }       
	   lineItemsMap.put('All',allProducts);       
    } 
    
    /**    
    * @Description: Method invoked from constructor and onchange events from UI that recalculates the "number of units" and "unit price" for the Summary statistics section
    * @Param: none
    * @Return: none
    */ 
    public void calculateSummaryStats(){
        noOfUnits = 0; //Initialise the total number of units to be displayed to 0
        perUnitPrice = 0; //Initialise the Per Unit Price to 0
        Decimal totalAmount = 0; //Variable to hold the Total Amount of all line items - this value has to be divided the number of units to achieve the average per unit price
        for(Lot__c lot : productLotRecords){
        	if(unitsPerProductMap.get(lot.Id) <> 0){
        		totalAmount = totalAmount + lot.Unit_Price__c * unitsPerProductMap.get(lot.Id);
        		noOfUnits += unitsPerProductMap.get(lot.Id);
        	}
        }
	    if(noOfUnits <> 0){//Obtaoin the average Per Unit Price of the order by dividing the Total Amount obtained by the Total Number of units calculated
	    	perUnitPrice = totalAmount/noOfUnits;
	    }              
    }   
    
    /**    
    * @Description: Method invoked from the "Save" button on the UI. This method inserts/updated purchase order, categorises the changes to line items from changes done and performs
    				the appropriate DMLs.    				
    * @Param: none
    * @Return: none
    */ 
    public PageReference savePurchaseOrder(){ 
    	try{ 
	        PageReference orderRedirect;
	        List<Line_Item__c> lineItemsToBeInserted = new List<Line_Item__c>(); //List of line items that have to be newly inserted for the purchase order
	        List<Line_Item__c> lineItemsToBeUpdated = new List<Line_Item__c>();  //List of line items whose units have been updated for the purchase order
	        List<Line_Item__c> lineItemsToBeDeleted = new List<Line_Item__c>();  //List of line items whose units have been updated to 0 and thus have to be deleted from the purchase order 
	        updateUnits();
	        calculateSummaryStats();
	        sp = Database.setSavepoint(); //Save point to roll back in case the purchase order DML succeeds and any of the line item DMLs fail
	        if(validateOrderFields()){
		        if(orderId <> null){//Update any changes made to Purchase Order attributes (Applicable to Draft and Open Status only)
		            purchaseOrder.Purchased__c = 0;//Explicitly setting to zero to avoid cyclic dependency - this field will automatically be updated from the line items trigger
		            update purchaseOrder;
		        }
		        else{ //Insert the purchase order in case of new order
		        	purchaseOrder.Id = null; 
		            insert purchaseOrder;		                        
		        }
		        for(Lot__c lot : productLotRecords){ //Iterate the wrapper records from the map to categorize each list item as to be inserted, updated or alerted
			    	if(lot.Line_Items__r.size() == 0 && 
			        	unitsPerProductMap.get(lot.Id) <> 0){//If there is no line item prior to save and the number of units is a non-zero value, add to the list that has to be inserted for the order
			            Line_Item__c li = new Line_Item__c(Order__c = purchaseOrder.Id,Lot__c = lot.Id,Units__c = unitsPerProductMap.get(lot.Id));
			            lineItemsToBeInserted.add(li);
			        }
			        else if(lot.Line_Items__r.size() > 0 && 
			        	unitsPerProductMap.get(lot.Id) == 0){//If there is an existing line item record and the units have been updated to 0, add to the ids that have to be deleted from the order
			            Line_Item__c li = new Line_Item__c(Id = lot.Line_Items__r[0].Id);
			            lineItemsToBeDeleted.add(li);
			        }
			        else if(lot.Line_Items__r.size() > 0 &&  
			        	unitsPerProductMap.get(lot.Id) <> 0){//If there is an existing line item record and  the Units have been updated to a non-zero value, add to the list to be updated
			            Line_Item__c li = new Line_Item__c(Id = lot.Line_Items__r[0].Id,Order__c = orderId,Lot__c = lot.Id,Units__c = unitsPerProductMap.get(lot.Id));
			            lineItemsToBeUpdated.add(li); 
			        }			                       
			    }
		        
		        if(!lineItemsToBeDeleted.isEmpty()){//If the list of Line items to be deleted is not empty, delete them with single delete DML
		        	delete lineItemsToBeDeleted;
		        }
		        if(!lineItemsToBeInserted.isEmpty()){//If the list of Line items to be inserted is not empty, insert them with single insert DML
		        	insert lineItemsToBeInserted;
		        }
		        if(!lineItemsToBeUpdated.isEmpty()){//If the list of Line items to be updated is not empty, update them with single update DML
		        	update lineItemsToBeUpdated;
		        }
		        orderId = purchaseOrder.Id;
		        orderRedirect = new PageReference('/'+purchaseOrder.Id); 
		        orderRedirect.setRedirect(true);
		        return orderRedirect;
	        }
    	}
    	catch(DMLException  e){
    		Database.rollback(sp);
    		//The debug statement below helps the admin/developer to capture the exact cause of failure when the  user tries to reproduce the issue
    		System.debug('Exception Message : '+e.getDMLMessage(0)+' Exception stack trace : '+e.getStackTraceString());
	    	ApexPages.Message Msg;
	    	if(e.getDMLType(0).equals(StatusCode.FIELD_CUSTOM_VALIDATION_EXCEPTION)){
	    		Msg = new ApexPages.Message(ApexPages.Severity.Error, e.getDMLMessage(0));
	    	}else{
	    		Msg = new ApexPages.Message(ApexPages.Severity.Error, Label.Unexpected_Error_Message);
	    	}
		    ApexPages.addMessage(Msg); 
    		
    	}
    	catch(Exception  e){
    		Database.rollback(sp);
    		//The debug statement below helps the admin/developer to capture the exact cause of failure when the  user tries to reproduce the issue
    		System.debug('Exception Message : '+e.getMessage()+' Exception stack trace : '+e.getStackTraceString());
    		ApexPages.Message Msg = new ApexPages.Message(ApexPages.Severity.Error,Label.Unexpected_Error_Message); 
	        ApexPages.addMessage(Msg);
    	}
    	return null;
    } 
    
    /**    
    * @Description: Method invoked on the onchange event of the product cateogry drop-down on the UI. This method inturn invokes the method that 
    				recalculates the summary statistics. 				
    * @Param: none
    * @Return: none
    */ 
    public boolean validateOrderFields() { 
        Boolean success = true;
        if(purchaseOrder.Name=='' || purchaseOrder.Name==null){
	        purchaseOrder.Name.addError(Label.Required_Field_Message);
	        if(!success){
		        ApexPages.Message Msg = new ApexPages.Message(ApexPages.Severity.Error,ConstantsUtility.PONAME+Label.Required_Field_Message);
		        ApexPages.addMessage(Msg);
	        }
	        success = false;	
	    }
	    if(purchaseOrder.Budget__c==null){
	    	purchaseOrder.Budget__c.addError(Label.Required_Field_Message);
	    	if(!success){	    	
		    	ApexPages.Message Msg = new ApexPages.Message(ApexPages.Severity.Error,ConstantsUtility.BUDGET+Label.Required_Field_Message);
		    	ApexPages.addMessage(Msg);
	    	}
	    	success = false;
	    }
	    if(purchaseOrder.Store__c==null){
	       	purchaseOrder.Store__c.addError(Label.Required_Field_Message);
	       	if(!success){
		       	ApexPages.Message Msg = new ApexPages.Message(ApexPages.Severity.Error,ConstantsUtility.STORE+Label.Required_Field_Message);
		       	ApexPages.addMessage(Msg);
	       	}
	       	success = false;
	    }      
	    return success;
    }
    
    /**    
    * @Description: Method invoked on the onchange event of the product cateogry drop-down on the UI. This method inturn invokes the method that 
    				recalculates the summary statistics. 				
    * @Param: none
    * @Return: none
    */ 
    public void getNewProductCategory() {  
    	updateUnits();
        recordList= new ApexPages.standardsetController(lineItemsMap.get(productCategory));       
        recordList.setPageSize(pageSize);
        pageNumber = recordList.getPageNumber();
		numOfPages = Integer.valueOf(Math.ceil(Decimal.valueOf(recordList.getResultSize())/Decimal.valueOf(recordList.getPageSize())));
        buildWrapperList(recordList.getRecords());        
        calculateSummaryStats();      
    }
    
    /**    
    * @Description: Pagination method which updates the map to maintain user input units and loads the first set of records from the standardsetcontroller 				
    * @Param: none
    * @Return: none
    */
    public void getFirst(){
    	updateUnits();
        recordList.first();
        pageNumber = recordList.getPageNumber();
        buildWrapperList(recordList.getRecords());
        calculateSummaryStats();
    }
    
    /**    
    * @Description: Pagination method which updates the map to maintain user input units and loads the last set of records from the standardsetcontroller 				
    * @Param: none
    * @Return: none
    */
    public void getLast(){
    	updateUnits();
        recordList.last();
        pageNumber = recordList.getPageNumber();
        buildWrapperList(recordList.getRecords());
        calculateSummaryStats();
    }    
    
    /**    
    * @Description: Pagination method which updates the map to maintain user input units and loads the previous set of records from the standardsetcontroller  				
    * @Param: none
    * @Return: none
    */
    public  void previousPage(){ 
    	updateUnits();
        recordList.previous();
        pageNumber = recordList.getPageNumber();
        buildWrapperList(recordList.getRecords());
        calculateSummaryStats();
    }
    
    /**    
    * @Description: Pagination method which updates the map to maintain user input units and loads the next set of records from the standardsetcontroller 				
    * @Param: none
    * @Return: none
    */
    public  void nextPage(){ 
    	updateUnits();
    	recordList.next();
    	pageNumber = recordList.getPageNumber();
        buildWrapperList(recordList.getRecords());
        calculateSummaryStats();
    }
    
    /**    
    * @Description: Method that sets the drop-down values of the "Product Category" from the lineitems available for filtering 				
    * @Param: none
    * @Return: none
    */
    public List<SelectOption> getProductCategories(){
        List<SelectOption> options = new List<SelectOption>();   
        options.add(new SelectOption(ConstantsUtility.ALL,ConstantsUtility.ANY_CATEGORY));
        for(String s: productCategories){ //The available values in the picklist should be only be the options for which product lot/line item records are available
           if(!s.equals(ConstantsUtility.ALL))
           	options.add(new SelectOption(s,s));
        }
        return options;
    } 
    
    /**    
    * @Description: Method that builds the required wrapper records from the product lot records - the list of wrappers built in this methodo are displayed in the UI 				
    * @Param: List of product lot records that have to built inot wrapper records 
    * @Return: none
    */
    public void buildWrapperList(List<Lot__c> productLots){
    	listWrappers = new List<LineItemsWrapper>();
    	for(Lot__c lot:productLots){
            LineItemsWrapper liw = new LineItemsWrapper(); 
            liw.productLot = lot;
            if(lot.Line_Items__r.size() > 0){                 
                liw.lineItem = lot.Line_Items__r[0];
                liw.lineItem.Units__c = unitsPerProductMap.get(lot.Id);
                listWrappers.add(liw);                
            }
            else{                
                liw.lineItem = new Line_Item__c();
                liw.lineItem.Units__c = unitsPerProductMap.get(lot.Id);
                listWrappers.add(liw);                
            }            
        }
        currentPage = listWrappers;
    }
    /**    
    * @Description: Method that updates the map to maintain the user inputs during pagination, change in product category filter etc. 				
    * @Param: none
    * @Return: none
    */
    public void updateUnits(){
    	for(LineItemsWrapper liw : currentPage){
    		unitsPerProductMap.put(liw.productLot.Id,Integer.valueOf(liw.lineItem.Units__c));
    	}
    }
    public class LineItemsWrapper{
        public Lot__c productLot {get; set;}       
        public Line_Item__c lineItem {get; set;}
        public LineItemsWrapper () {
        
        }
    }
}