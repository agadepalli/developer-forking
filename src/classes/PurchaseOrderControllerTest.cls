/** 
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition. 
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
 
@isTest(seeAllData=false)
private class PurchaseOrderControllerTest { 
    /*
     * Method to test the intialization and behaviour of constructor when the order in the context is one existing in database(scenario of clicking edit button)
    */
    static testmethod void constructorInitializatoinTest_existingPurchaseOrder(){
        User testAccountManager = UnitTestHelper.reusableUser('Account Managers','testAM');
        insert testAccountManager;
        User testInventoryManager = UnitTestHelper.reusableUser('Inventory Managers','testIM');
        insert testInventoryManager;
        System.runAs(testInventoryManager){
            Lot__c prod1 = UnitTestHelper.reusableProductLot(100,5,'Batteries','HTC');
            insert prod1;
        }
        System.runAs(testAccountManager){           
            Account acc = UnitTestHelper.reusableAccount();
            insert acc;
            Order__c odr = UnitTestHelper.reusablePurchaseOrder('Order 1',acc.Id,100000, 'Open');
            insert odr;
            Test.startTest();
            	PageReference pRef = Page.PurchaseOrder;
        		Test.setCurrentPage(pRef);
                ApexPages.StandardController sc = new ApexPages.StandardController(odr);
                PurchaseOrderController poc = new PurchaseOrderController(sc);
                System.assertEquals(poc.productCategory,'All');
                System.assertEquals(poc.purchaseOrder.Status__c,'Open');
            Test.stopTest();            
        }
    }
    /*
     * Method to test the intialization and behaviour of constructor when the order in the context is new purchase order(scenario of clicking new button)
    */
    static testmethod void constructorInitializatoinTest_newPurchaseOrder(){
        User testAccountManager = UnitTestHelper.reusableUser('Account Managers','testAM');
        insert testAccountManager;
        System.runAs(testAccountManager){
            Account acc = UnitTestHelper.reusableAccount();
            insert acc;
            Order__c odr = UnitTestHelper.reusablePurchaseOrder('Order 1',acc.Id,100000, 'Open');
            Test.startTest();
            	PageReference pRef = Page.PurchaseOrder;
        		Test.setCurrentPage(pRef);
                ApexPages.StandardController sc = new ApexPages.StandardController(odr);
                PurchaseOrderController poc = new PurchaseOrderController(sc);
                System.assertEquals(poc.productCategory,'All');
                System.assertEquals(poc.purchaseOrder.Status__c,'Draft');
            Test.stopTest();
        }
    }
    
    /*
     * Method to test the insertion of new purchase order when "Save" button is clicked in UI
    */
    static testmethod void insertPurchaseOrderTest(){
        User testAccountManager = UnitTestHelper.reusableUser('Account Managers','testAM');
        insert testAccountManager;
        System.runAs(testAccountManager){
            Account acc = UnitTestHelper.reusableAccount();
            insert acc;
            Order__c odr = UnitTestHelper.reusablePurchaseOrder('Order 1',acc.Id,100000, 'Open');
            Test.startTest();
            	PageReference pRef = Page.PurchaseOrder;
        		Test.setCurrentPage(pRef);
                ApexPages.StandardController sc = new ApexPages.StandardController(odr);
                PurchaseOrderController poc = new PurchaseOrderController(sc);
                System.assertEquals(poc.productCategory,'All');
                System.assertEquals(poc.purchaseOrder.Status__c,'Draft');
                poc.purchaseOrder.Store__c = acc.Id;
                poc.purchaseOrder.Budget__c = 250000;
                poc.purchaseOrder.Name = 'Test Purchase Order';
                poc.savePurchaseOrder();
                Order__c insertedOrder = [Select Id from Order__c where Id = :poc.purchaseOrder.Id];
            Test.stopTest();
        }
    }

    /*
     * Method to test the "Save" funcationaliy of an existing purchase order and creation of new line item records from product lots available
    */
    static testmethod void savePurchaseOrder(){
        User testAccountManager = UnitTestHelper.reusableUser('Account Managers','testAM');
        insert testAccountManager;
        User testInventoryManager = UnitTestHelper.reusableUser('Inventory Managers','testIM');
        insert testInventoryManager;
        List<Lot__c> batteryProducts;
        System.runAs(testInventoryManager){
            batteryProducts = new List<Lot__c>();
            for(Integer i=0;i<15;i++){
                Lot__c prod1 = UnitTestHelper.reusableProductLot(100,5,'Batteries','HTC');
                batteryProducts.add(prod1);
            }
            insert batteryProducts;
        }
        System.runAs(testAccountManager){
            
            List<Line_Item__c> batteryListItems = new List<Line_Item__c>();
            Account acc = UnitTestHelper.reusableAccount();
            insert acc;
            Order__c odr = UnitTestHelper.reusablePurchaseOrder('Order 1',acc.Id,100000, 'Draft');
            insert odr;     
            
            for(Integer i =0;i<5;i++){
                Line_Item__c lItem1 = UnitTestHelper.reusableLineItem(batteryProducts[i].Id,odr.Id,50); 
                batteryListItems.add(lItem1);
            }
            insert batteryListItems;
            Test.startTest();
            	PageReference pRef = Page.PurchaseOrder;
        		Test.setCurrentPage(pRef);
                ApexPages.StandardController sc = new ApexPages.StandardController(odr);
                PurchaseOrderController poc = new PurchaseOrderController(sc);
                poc.purchaseOrder.Name = 'Test Purchase Order';
                System.assertEquals(poc.numOfPages,3);
                poc.purchaseOrder.Budget__c = 2500000;
                for(String s: poc.unitsPerProductMap.keySet()){
                    poc.unitsPerProductMap.put(s,100);
                }
                for(PurchaseOrderController.LineItemsWrapper liw : poc.currentPage){
                    liw.lineItem.Units__c = 75;
                }
                poc.savePurchaseOrder();
                List<Line_Item__c> insertedLineItems = [Select Id from Line_Item__c where Order__c = :odr.Id];
                System.assertEquals(insertedLineItems.size(),15);
                List<Lot__c> updatedLots = [Select Remaining__c from Lot__c where Remaining__c = 0];
                System.assertEquals(updatedLots.size(),10);//only 10 line items created in the test class scope has all the remianing units assigned - This will make sure that 10 records were succesfully inserted
            Test.stopTest();            
        }
    }
    /*
     * Method to test the "Save" funcationaliy of an existing purchase order and deleting exisiting line item records when number of units is updated to 0
    */
    static testmethod void savePurchaseOrder_deleteTest(){
        User testAccountManager = UnitTestHelper.reusableUser('Account Managers','testAM');
        insert testAccountManager;
        User testInventoryManager = UnitTestHelper.reusableUser('Inventory Managers','testIM');
        insert testInventoryManager;
        List<Lot__c> batteryProducts;
        System.runAs(testInventoryManager){
            batteryProducts = new List<Lot__c>();
            for(Integer i=0;i<15;i++){
                Lot__c prod1 = UnitTestHelper.reusableProductLot(100,5,'Batteries','HTC');
                batteryProducts.add(prod1);
            }
            insert batteryProducts;
        }
        System.runAs(testAccountManager){
            List<Line_Item__c> batteryListItems = new List<Line_Item__c>();
            Account acc = UnitTestHelper.reusableAccount();
            insert acc;
            Order__c odr = UnitTestHelper.reusablePurchaseOrder('Order 1',acc.Id,100000, 'Draft');
            insert odr;

            for(Integer i =0;i<5;i++){
                Line_Item__c lItem1 = UnitTestHelper.reusableLineItem(batteryProducts[i].Id,odr.Id,50); 
                batteryListItems.add(lItem1);
            }
            insert batteryListItems;
            Test.startTest();
            	PageReference pRef = Page.PurchaseOrder;
        		Test.setCurrentPage(pRef);
                ApexPages.StandardController sc = new ApexPages.StandardController(odr);
                PurchaseOrderController poc = new PurchaseOrderController(sc);
                System.assertEquals(poc.numOfPages,3);
                poc.purchaseOrder.Budget__c = 2500000;
                poc.purchaseOrder.Name = 'Test Purchase Order';
                for(String s: poc.unitsPerProductMap.keySet()){
                    poc.unitsPerProductMap.put(s,0);
                }
                for(PurchaseOrderController.LineItemsWrapper liw : poc.currentPage){
                    liw.lineItem.Units__c =0;
                }                
                poc.savePurchaseOrder();
                List<Line_Item__c> deletedLineItems = [Select Id from Line_Item__c where Order__c = :odr.Id];
                System.assertEquals(deletedLineItems.size(),0);             
            Test.stopTest();            
        }
    }
    
    /*
     * Method to test the "Save" funcationaliy of an existing purchase order and updating the units of exisiting line item records when number of units is updated to a non-zero value
    */
    static testmethod void savePurchaseOrder_updateTest(){
        User testAccountManager = UnitTestHelper.reusableUser('Account Managers','testAM');
        insert testAccountManager;
        User testInventoryManager = UnitTestHelper.reusableUser('Inventory Managers','testIM');
        insert testInventoryManager;
        List<Lot__c> batteryProducts;
        System.runAs(testInventoryManager){
            batteryProducts = new List<Lot__c>();
            for(Integer i=0;i<15;i++){
                Lot__c prod1 = UnitTestHelper.reusableProductLot(100,5,'Batteries','HTC');
                batteryProducts.add(prod1);
            }
            insert batteryProducts;
        }
        System.runAs(testAccountManager){
            
            List<Line_Item__c> batteryListItems = new List<Line_Item__c>();
            Account acc = UnitTestHelper.reusableAccount();
            insert acc;
            Order__c odr = UnitTestHelper.reusablePurchaseOrder('Order 1',acc.Id,100000, 'Draft');
            insert odr;
            for(Integer i =0;i<5;i++){
                Line_Item__c lItem1 = UnitTestHelper.reusableLineItem(batteryProducts[i].Id,odr.Id,50); 
                batteryListItems.add(lItem1);
            }
            insert batteryListItems;
            Test.startTest();
            	PageReference pRef = Page.PurchaseOrder;
        		Test.setCurrentPage(pRef);
                ApexPages.StandardController sc = new ApexPages.StandardController(odr);
                PurchaseOrderController poc = new PurchaseOrderController(sc);
                System.assertEquals(poc.numOfPages,3);
                poc.purchaseOrder.Name = 'Test Purchase Order';
                poc.purchaseOrder.Budget__c = 2500000;
                for(String s: poc.unitsPerProductMap.keySet()){
                    poc.unitsPerProductMap.put(s,75);
                }
                for(PurchaseOrderController.LineItemsWrapper liw : poc.currentPage){
                    liw.lineItem.Units__c =0;
                }
                poc.savePurchaseOrder();
                List<Line_Item__c> updatedLineItems = [Select Id,Units__c from Line_Item__c where Order__c = :odr.Id AND Units__c = 75];
                System.assertEquals(updatedLineItems.size(),10);             
            Test.stopTest();            
        }
    }
    
    /*
     * Method to test that only "Product Categories" from product lots/existing line items are available for selection
    */
    static testmethod void getProductCategoriesTest(){ 
        User testAccountManager = UnitTestHelper.reusableUser('Account Managers','testAM');
        insert testAccountManager;
        User testInventoryManager = UnitTestHelper.reusableUser('Inventory Managers','testIM');
        insert testInventoryManager;
        List<Lot__c> batteryProducts;
        System.runAs(testInventoryManager){
            batteryProducts = new List<Lot__c>();
            for(Integer i=0;i<15;i++){
                Lot__c prod1 = UnitTestHelper.reusableProductLot(100,5,'Batteries','HTC');
                batteryProducts.add(prod1);
            }
            insert batteryProducts;
        }
        System.runAs(testAccountManager){
            Account acc = UnitTestHelper.reusableAccount();
            insert acc;
            Order__c odr = UnitTestHelper.reusablePurchaseOrder('Order 1',acc.Id,100000, 'Draft');
            Test.startTest();
            	PageReference pRef = Page.PurchaseOrder;
        		Test.setCurrentPage(pRef);
                ApexPages.StandardController sc = new ApexPages.StandardController(odr);
                PurchaseOrderController poc = new PurchaseOrderController(sc);
                System.assertEquals(poc.numOfPages,3);
                poc.getProductCategories();
                poc.productCategory = 'Batteries';
                poc.getNewProductCategory();
                System.assertEquals(poc.pageNumber,1);
            Test.stopTest();
        }
    }
    
    /*
     * Method to test that the right set of records are displayed on the current page after pagination
    */
    static testmethod void getCurrentPageRecordsTest(){
        User testAccountManager = UnitTestHelper.reusableUser('Account Managers','testAM');
        insert testAccountManager;
        User testInventoryManager = UnitTestHelper.reusableUser('Inventory Managers','testIM');
        insert testInventoryManager;
        List<Lot__c> batteryProducts;
        System.runAs(testInventoryManager){
            batteryProducts = new List<Lot__c>();
            for(Integer i=0;i<15;i++){
                Lot__c prod1 = UnitTestHelper.reusableProductLot(100,5,'Batteries','HTC');
                batteryProducts.add(prod1);
            }
            insert batteryProducts;
        }
        System.runAs(testAccountManager){
            List<Line_Item__c> batteryListItems = new List<Line_Item__c>();
            Account acc = UnitTestHelper.reusableAccount();
            insert acc;
            Order__c odr = UnitTestHelper.reusablePurchaseOrder('Order 1',acc.Id,100000, 'Draft');
            insert odr;
            Test.startTest();
            	PageReference pRef = Page.PurchaseOrder;
        		Test.setCurrentPage(pRef);
                ApexPages.StandardController sc = new ApexPages.StandardController(odr);
                PurchaseOrderController poc = new PurchaseOrderController(sc);
                System.assertEquals(poc.currentPage.size(),5);//only 10 line items created in the test class scope has all the remianing units assigned - This will make sure that 10 records were succesfully inserted
            Test.stopTest();            
        }
    }
    
    /*
     * Method to test that the navigation controls of pagination 
    */
    static testmethod void paginationTests(){
        User testAccountManager = UnitTestHelper.reusableUser('Account Managers','testAM');
        insert testAccountManager;
        User testInventoryManager = UnitTestHelper.reusableUser('Inventory Managers','testIM');
        insert testInventoryManager;
        List<Lot__c> batteryProducts;
        System.runAs(testInventoryManager){
            batteryProducts = new List<Lot__c>();
            for(Integer i=0;i<15;i++){
                Lot__c prod1 = UnitTestHelper.reusableProductLot(100,5,'Batteries','HTC');
                batteryProducts.add(prod1);
            }
            insert batteryProducts;
        }
        System.runAs(testAccountManager){
            Account acc = UnitTestHelper.reusableAccount();
            insert acc;
            Order__c odr = UnitTestHelper.reusablePurchaseOrder('Order 1',acc.Id,100000, 'Draft');

            Test.startTest();
            	PageReference pRef = Page.PurchaseOrder;
        		Test.setCurrentPage(pRef);
                ApexPages.StandardController sc = new ApexPages.StandardController(odr);
                PurchaseOrderController poc = new PurchaseOrderController(sc);
                System.assertEquals(poc.numOfPages,3);
                System.assertEquals(poc.pageNumber,1);
                poc.getLast();
                System.assertEquals(poc.pageNumber,3);
                poc.getFirst();
                System.assertEquals(poc.pageNumber,1);
                System.assert(poc.hasNext);
                System.assert(!poc.hasPrevious);
                poc.nextPage();
                System.assertEquals(poc.pageNumber,2);
                System.assert(poc.hasNext);
                System.assert(poc.hasPrevious);
                poc.previousPage();
                System.assertEquals(poc.pageNumber,1);
                System.assert(poc.hasNext);
                System.assert(!poc.hasPrevious);
            Test.stopTest();
        }       
    }
    /*
     * Method to test the that the appropriate message is displayed when the budget is less that 100k$
    */
    static testmethod void insertNewPurchaseOrder_LowBudgetNegativeScenario(){
        User testAccountManager = UnitTestHelper.reusableUser('Account Managers','testAM');
        insert testAccountManager;
        System.runAs(testAccountManager){
            Account acc = UnitTestHelper.reusableAccount();
            insert acc;
            Order__c odr = UnitTestHelper.reusablePurchaseOrder('Order 1',acc.Id,100000, 'Open');
            Test.startTest();
            	PageReference pRef = Page.PurchaseOrder;
        		Test.setCurrentPage(pRef);
                ApexPages.StandardController sc = new ApexPages.StandardController(odr);
                PurchaseOrderController poc = new PurchaseOrderController(sc);
                poc.purchaseOrder.Store__c = acc.Id;
                poc.purchaseOrder.Budget__c = 25000;
                poc.purchaseOrder.Name = 'Test Purchase Order';
                poc.savePurchaseOrder();
                List<Order__c> insertedOrders = [Select Id from Order__c];
                System.assertEquals(insertedOrders.size(),0);
                System.assert(ApexPages.hasMessages());
                System.assertEquals(ApexPages.getMessages()[0].getSummary(),'Minimum $100k');
            Test.stopTest();
        }
    }
    /*
     * Method to test the that the appropriate message is displayed when the status is cleared and tried to save
    */
    static testmethod void insertNewPurchaseOrder_NoStatusNegativeScenario(){
        User testAccountManager = UnitTestHelper.reusableUser('Account Managers','testAM');
        insert testAccountManager;
        System.runAs(testAccountManager){
            Account acc = UnitTestHelper.reusableAccount();
            insert acc;
            Order__c odr = UnitTestHelper.reusablePurchaseOrder('Order 1',acc.Id,100000, 'Open');
            Test.startTest();
            	PageReference pRef = Page.PurchaseOrder;
        		Test.setCurrentPage(pRef);
                ApexPages.StandardController sc = new ApexPages.StandardController(odr);
                PurchaseOrderController poc = new PurchaseOrderController(sc);
                poc.purchaseOrder.Store__c = acc.Id;
                poc.purchaseOrder.Budget__c = 250000;
                poc.purchaseOrder.Name = 'Test Purchase Order';
                poc.purchaseOrder.Status__c = '';
                poc.savePurchaseOrder();
                List<Order__c> insertedOrders = [Select Id from Order__c];
                System.assertEquals(insertedOrders.size(),0);
                System.assert(ApexPages.hasMessages());
                System.assertEquals(ApexPages.getMessages()[0].getSummary(),'A value is required');
            Test.stopTest();
        }
    }
    
    /*
     * Method to test the that the appropriate message is displayed when the budget is edited when the status is not "Draft"
    */
    static testmethod void insertNewPurchaseOrder_UpdateBudgetNegativeScenario(){
        User testAccountManager = UnitTestHelper.reusableUser('Account Managers','testAM');
        insert testAccountManager;
        System.runAs(testAccountManager){
            Account acc = UnitTestHelper.reusableAccount();
            insert acc;
            Order__c odr = UnitTestHelper.reusablePurchaseOrder('Order 1',acc.Id,100000, 'Open');
            Test.startTest();
            	PageReference pRef = Page.PurchaseOrder;
        		Test.setCurrentPage(pRef);
                ApexPages.StandardController sc = new ApexPages.StandardController(odr);
                PurchaseOrderController poc = new PurchaseOrderController(sc);
                poc.purchaseOrder.Store__c = acc.Id;
                poc.purchaseOrder.Budget__c = 250000;
                poc.purchaseOrder.Status__c = 'Open';
                poc.purchaseOrder.Name = 'Test Purchase Order'; 
                poc.savePurchaseOrder();
                List<Order__c> insertedOrders = [Select Id from Order__c];
                System.assertEquals(insertedOrders.size(),1);
                poc.purchaseOrder.Budget__c=100000;
                poc.savePurchaseOrder();
            Test.stopTest();
            System.assert(ApexPages.hasMessages());
            System.assertEquals(ApexPages.getMessages()[0].getSummary(),'Budget only editable for draft purchase orders');          
        }
    }
    
    /*
     * Method to test that the behaviour of controller when save button is clicked without all the required fields on the order
    */
    static testmethod void testRequiredFieldsMissingScenarios(){
        User testAccountManager = UnitTestHelper.reusableUser('Account Managers','testAM');
        insert testAccountManager;
        System.runAs(testAccountManager){
            Account acc = UnitTestHelper.reusableAccount();
            insert acc;
            Order__c odr = UnitTestHelper.reusablePurchaseOrder('Order 1',acc.Id,100000, 'Open');
            Test.startTest();
            	PageReference pRef = Page.PurchaseOrder;
        		Test.setCurrentPage(pRef); 
                ApexPages.StandardController sc = new ApexPages.StandardController(odr);
                PurchaseOrderController poc = new PurchaseOrderController(sc);
                poc.purchaseOrder.Store__c = null;
                poc.purchaseOrder.Budget__c = null;
                poc.purchaseOrder.Status__c = null;
                poc.purchaseOrder.Name = null; 
                poc.savePurchaseOrder();
                List<Order__c> insertedOrders = [Select Id from Order__c];
                System.assertEquals(insertedOrders.size(),0);
            Test.stopTest();
            System.assert(ApexPages.hasMessages()); 
        }
    }
}