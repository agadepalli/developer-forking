/* @Class Name   : UnitTestHelper
 * @Description  : Class that contains methods for building reusable test data for scenario based testing in test classes/methods
 * @Created By   : Abhinav Gadepalli
 * @Created On   : 10/2/2014
 * @Modification Log:  
 * -------------------------------------------------------------------------------------------------------
 * @Developer                Date                 Description 
 * -------------------------------------------------------------------------------------------------------
 * @Abhinav G                10/08/2014           Created
 * --------------------------------------------------------------------------------------------------------
 */
@isTest
//Removing without sharing as this is test class and does not have any impact on the core functionality
public class UnitTestHelper {
	/*
	 * Reusable method to build and return a purchase order with Order NAme, Store Name, Budget and status
	*/
	public static Order__c reusablePurchaseOrder(String orderName, Id AccountId,Decimal budget, String status){
		Order__c o = new Order__c();
		o.Name = orderName;
		o.Budget__c = budget;
		o.Status__c = status;
		o.Store__c = AccountId;
		return o;
	}
	/*
	 * Reusable method to build and return an account
	*/
	public static Account reusableAccount(){
		Account acc = new Account();
		acc.Name='Test Account';
		return acc;	
	}
	
	/*
	 * Reusable method to build and return a product with Name, category, available untis and unit price
	*/
	public static Lot__c reusableProductLot(Integer units, Decimal unitPrice, String productCategory, String productName){
		Lot__c lot = new Lot__c();
		lot.Product__c = productName;
		lot.Unit_Price__c = unitPrice;
		lot.Product_Category__c = productCategory;
		lot.Lot_Size__c = units;
		return lot;
	}
	
	/*
	 * Reusable method to build and return a Line Item with relationships with Order and product.
	*/
	public static Line_Item__c reusableLineItem(Id prodLotId,Id orderId,Integer units){
		Line_Item__c lItem = new Line_Item__c();
		lItem.Lot__c = prodLotId;
		lItem.Order__c = orderId;
		lItem.Units__c = units;
		return lItem;
	}
	
	/*
	 * Reusable method to create a user based on profile sent as parameter
    */
    public static User reusableUser(String profilename,String alias){ 
        //Create a User Record in Test Class
        Profile p = [SELECT Id FROM Profile WHERE Name=:profilename];
        String orgId = UserInfo.getOrganizationId();
        String dateString = String.valueof(Datetime.now()).replace(' ','').replace(':','').replace('-','');
        Integer randomInt = Integer.valueOf(math.rint(math.random()*1000000));
        String uniqueName = orgId + dateString + randomInt;        
        User u = new User(Alias = alias, Email=alias+'test@certification.com', 
                EmailEncodingKey='UTF-8', LastName='Testing'+profilename, LanguageLocaleKey='en_US', 
                LocaleSidKey='en_US', ProfileId = p.Id, 
                TimeZoneSidKey='America/Los_Angeles', Username = uniqueName + '@certification' + orgId + '.org'); 
        return u;
    }
}